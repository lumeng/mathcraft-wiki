== 使用模式的系统内置函数 ==

我们来看几个可将模式作为参数的内置函数。其中一个是我们已经讨论过的 MatchQ。

=== Cases ===

此函数用于表达式拆解。更确切的说，是用来搜索表达式中满足特定模式的子表达式。

Cases[expr, pattern] 返回 <expr> 的首层中满足 <pattern> 的全部表达式。可选的第三个参数是层级指定，它可以是个整数（正负皆可，包括无穷大Infinity），也可以是一对花括号中的一个整数，或者花括号中的一对整数。第一种情况下 Cases 搜索每层表达式，直到参数所指定的那一层（从表达式的顶层还是底层开始的问题取决于参数的正负），第二种情况下它仅搜索参数所指定的层， 而在第三种情况下它搜索列表里两数字所指定的层数区间中的所有层。某个整数还可以作为其可选的第四个参数，用来指定 Cases 找到多少个结果才会停止。如果没有这个参数 Cases 便会找出给定层上的全部结果。

下面是一些例子：

==== 例子：找出列表中的整数 ====

作为最简单的例子，让我们从一个一维列表中选出所有整数：

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];

testlist= {3/2,Pi,3,1.4142135,10,99.99,15,25};</nowiki>}}</code>

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,_Integer]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{3,10,15,25}</nowiki>}}
</code>

注意在这里不必为此模式附上标签，因为我们不需要对结果进行任何转换。

==== 例子：过滤数据 ====

我们来考虑更实际点的例子：假设我们要移除一些数据中小于临界值<eps>的数据点。

<code>
{{SourceCodeIn|code=<nowiki>eps = 0.3;

data= Table[Random[],{10}](*编译者按：新版本中推荐用RandomReal[1,10]这种写法，下同*)</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{0.0985434,0.539199,0.283028,0.138057,0.634992,0.694699,0.854314,0.45518,0.701924,0.189933}</nowiki>}}
</code>

如果要用 Cases 实现此功能则可以这样写：

<code>
{{SourceCodeIn|code=<nowiki>Cases[data,x_/;x>eps]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{0.539199,0.634992,0.694699,0.854314,0.45518,0.701924}</nowiki>}}
</code>

==== 扩展功能 ====

Cases 的另外一项功能是对找到的结果立即实施转换。其语法是 Cases[expr, rule, levespec, n]，最后两个可选参数和之前一样，但是在原来模式（pattern）的位置现在是一条规则（rule），它以某个模式作为左值。例如我们想根据该数字是否能被5整除，在每个找到的数字后面加上 True 或 False。

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_Integer:>{x,Mod[x,5]==0}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{3,False},{10,True},{15,True},{25,True}}</nowiki>}}
</code>

注意这里 Cases 生成了一个列表。我们把这种列表生成称作动态列表生成，而不是前面章节提到的"静态"列表生成。

==== 例子：选出列表中的整数 ====

我们来生成一列正负随机整数：

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];

testlist= Table[Random[Integer,{-10,10}],{15}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{-9,9,-9,7,-2,-3,-5,4,-7,10,-2,7,7,9,5}</nowiki>}}
</code>

为了取出其中的正数，我们需要的模式是 _?Positive

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,_?Positive]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{9,7,4,10,7,7,9,5}</nowiki>}}
</code>

仅仅取出大于5的数：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_/;x>5]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{9,7,10,7,7,9}</nowiki>}}
</code>

或者是能被3整除的数：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_/;Mod[x,3]==0]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{-9,9,-9,-3,9}</nowiki>}}
</code>

最后两个例子使用了条件模式。

==== 例子：嵌套列表 ====

我们来考虑一个更复杂的 2 层列表：

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];

testlist=Table[i+j,{i,1,4},{j,1,3}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,3,4},{3,4,5},{4,5,6},{5,6,7}}</nowiki>}}
</code>

假设现在要找出全部偶数，我们来试试：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_?EvenQ]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{}</nowiki>}}
</code>

没成功\[Ellipsis]\[Ellipsis]这是因为默认情况下Cases只会检测表达式的首层，在这里仅仅是一些子列表，而不是数字。

<code>
{{SourceCodeIn|code=<nowiki>FullForm[testlist]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>List[List[2,3,4],List[3,4,5],List[4,5,6],List[5,6,7]]</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>List[List[2,3,4],List[3,4,5],List[4,5,6],List[5,6,7]] </nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,3,4},{3,4,5},{4,5,6},{5,6,7}}</nowiki>}}
</code>

正确的搜索方法是这样的：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_?EvenQ,2]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{2,4,4,4,6,6}</nowiki>}}
</code>

这里的参数<2>指示 Cases 一直搜索到第 2 层（包括 2 层自己），也就是首层和次层。如果我们想只搜索第二层的话，就必须将其放入花括号（列表）中：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_?EvenQ,{2}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{2,4,4,4,6,6}</nowiki>}}
</code>

在上面的例子中两种方式的效果是一样的。现在假设我们想找出所有的子列表，那么应该这样做：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,_List]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,3,4},{3,4,5},{4,5,6},{5,6,7}}</nowiki>}}
</code>

结果看起来的初始列表是一样的，但实际上它是由所有找出的子列表组成的，在这里的确和初始列表是一致的。这一点很容易用对结果的转换证明：

<code>
{{SourceCodeIn|code=<nowiki>Clear[found];

Cases[testlist,x_List:>found[x]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{found[{2,3,4}],found[{3,4,5}],found[{4,5,6}],found[{5,6,7}]}</nowiki>}}
</code>

我们可以对子列表附加一些条件\[LongDash]\[LongDash]比如仅仅找出包含数字4的子列表：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_List/;Cases[x,4]=!={}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,3,4},{3,4,5},{4,5,6}}</nowiki>}}
</code>

此例说明了嵌套使用 Cases 的可能性（然而对于这个例子，还有比此法更高效的方法，比如使用下面将提到的 MemberQ 函数）。

如果现在我们将搜索范围限制在第二层，

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,_List,{2}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{}</nowiki>}}
</code>

由于在第二层不存在任何（子）列表，我们找不到任何结果。

==== 例子：第三章中奇子列表问题的优雅解决方法 ====

作为结合使用 Cases 和条件模式的例子，我们可以回过头来看看从列表中提取出包含奇数个奇数的子列表的问题。在此之前我们曾考虑过程式、结构式和函数式实现（章节 3.6.8.4）。我们现在的方法将完全基于模式。

如下是我们的测试列表：

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];

testlist=Range/@Range[6]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{1},{1,2},{1,2,3},{1,2,3,4},{1,2,3,4,5},{1,2,3,4,5,6}}</nowiki>}}
</code>

这是我们的解决方法：

<code>
{{SourceCode|code=<nowiki>Cases[testlist,x_List/;OddQ[Count[x,_?OddQ]]]</nowiki>}}
</code>

<code>
{{SourceCodeOut|code=<nowiki>{{1},{1,2},{1,2,3,4,5},{1,2,3,4,5,6}}</nowiki>}}
</code>

在我看来，这是解决此问题更优雅的途径。另外，实现过程也十分透明：我们用命令 Count[x, _?OddQ] 得到每个子列表中奇数的个数（Count 命令将马上介绍），然后我们再检查得到的数字是否为奇数。我要提醒你 /; 符号是条件操作符的简写。

稍加修改我们就可以将每一个找到的子列表和其中偶数的个数一并返回：

<code>
{{SourceCodeIn|code=<nowiki>Cases[testlist,x_List/;OddQ[Count[x,_?OddQ]]:>{Count[x,_?EvenQ],x}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{0,{1}},{1,{1,2}},{2,{1,2,3,4,5}},{3,{1,2,3,4,5,6}}}</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];</nowiki>}}</code>

==== 为什么不用循环？ ====

有人可能认为上面给出的所有例子都可以用循环来完成。非也。我至少有三个理由：

1. Cases 对于列表的生成做了优化\[LongDash]\[LongDash]列表是在内部生成的。 就像我们在 3. 4.5.3 看到的那样， 在循环中生成列表是极其低效的。

2. Cases 可以使用模式，它对元素的选择基于模式匹配而不是简单的比较。当然当我们仅仅是查找某个固定的对象（不包含模式）的时候，模式匹配就和相同性比较没什么两样了。

3. Cases 作用于通用 Mathematica 表达式（表达式树），而且我们可以指定搜索在哪些层上进行。这在过程式方法中将需要多重循环来完成。

==== 例子：收集两变量多项式的项 ====

我们可以给出更多的例子。我们应当记住的是，Cases 是个万能命令，它可作用于通用 Mathematica 表达式（表达式树），特别是作用于列表。为了阐明 Cases 的通用性，我们将其用于找出多项式(1+x)^10 * (1+y)^10 中y的次数为偶数x的次数为奇数的项：

<code>
{{SourceCodeIn|code=<nowiki>Clear[a,x,y];

Cases[Expand[(1+x)^10*(1+y)^10],a_*x^_?OddQ*y^_?EvenQ,Infinity]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{5400 x^3 y^2,11340 x^5 y^2,5400 x^7 y^2,450 x^9 y^2,25200 x^3 y^4,52920 x^5 y^4,25200 x^7 y^4,2100 x^9 y^4,25200 x^3 y^6,52920 x^5 y^6,25200 x^7 y^6,2100 x^9 y^6,5400 x^3 y^8,11340 x^5 y^8,5400 x^7 y^8,450 x^9 y^8,120 x^3 y^10,252 x^5 y^10,120 x^7 y^10,10 x^9 y^10}</nowiki>}}
</code>

最后的参数设为 Infinity 表示表达式中所有的层都会被搜索。

=== DeleteCases ===

这个函数的功能看看其名称就很清楚，它删除表达式中满足某给定模式的所有成员。它的语法与 Cases 类似。最主要也最重要的区别是 Cases 返回的是找到的子表达式组成的列表，而 DeleteCases 返回的是删除了这些子表达式的原列表拷贝。

==== 例子：删除列表中的奇数 ====

我们将删除某个列表中的全部奇数：

<code>
{{SourceCodeIn|code=<nowiki>testlist=Range[15];

DeleteCases[Range[15],_?OddQ]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{2,4,6,8,10,12,14}</nowiki>}}
</code>

我们刚刚移除了列表中的全部奇数。但是这并不意味着 <testlist> 本身会发生任何变化：

<code>
{{SourceCodeIn|code=<nowiki>testlist</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}</nowiki>}}
</code>

在这方面，DeleteCases 和大多数 Mathematica 内置函数一样\[LongDash]\[LongDash]没有副作用： 输入变量的拷贝会被生成和修改。如果我们想改变 <testlist> 的内容，则必须这样写：

<code>
{{SourceCodeIn|code=<nowiki>testlist=DeleteCases[testlist,_?OddQ]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{2,4,6,8,10,12,14}</nowiki>}}
</code>

我们来进行一项小规模的时间测试：测量 DeleteCases 删除前100000个自然数中被5除的余数小于2的项所用的时间：

<code>
{{SourceCodeIn|code=<nowiki>Timing[Short[DeleteCases[Range[100000],x_/;Mod[x,5]<= 2]]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{0.296402,{3,4,8,9,13,14,18,19,<<39984>>,99983,99984,99988,99989,99993,99994,99998,99999}}</nowiki>}}
</code>

下面是直接的过程式实现：

<code>
{{SourceCode|code=<nowiki>Module[{i, j, result, starting},
	    For[i = j = 1; starting = result = Range[100000],
		    i <= 100000, i ++, If[Mod[starting[[i]], 5] <= 2,
			    result[[j ++]] = starting[[i]]]];
		    Take[result, j - 1]] // Short // Timing</nowiki>}}
</code>

<code>
{{SourceCodeOut|code=<nowiki>{0.967206,{1,2,5,6,7,10,11,12,<<59984>>,99987,99990,99991,99992,99995,99996,99997,100000}}</nowiki>}}
</code>

此方法需要额外的辅助变量，我得用 Module （后面将介绍）将它们局部化，而且代码长了5倍慢了若干倍。这再次证明了在 Mathematica 中使用列表的主要规则—避免将它们打成碎片。

==== 例子：非零整数序列 ====

考虑下面的问题：我们被给定一整数列表，有些元素可能为 0。任务是提取出列表中所有的非零连续子序列。	

例如，下面将是我们的测试数据集：

<code>
{{SourceCodeIn|code=<nowiki>testdata=Table[Random[Integer,{0,3}],{20}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{0,2,2,2,1,2,0,0,1,1,0,0,1,2,0,1,3,1,1,2}</nowiki>}}
</code>

解决此问题的第一步要用到 Split （章节 3.10.3） ，每遇到 0 便将列表拆分为子列表：

<code>
{{SourceCodeIn|code=<nowiki>step1 = Split[testdata,#1!=0&]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{0},{2,2,2,1,2,0},{0},{1,1,0},{0},{1,2,0},{1,3,1,1,2}}</nowiki>}}
</code>

然后我们的子列表中却包含了一些 0 ，所以我们必须将它们删除（注意层级指定）：

<code>
{{SourceCodeIn|code=<nowiki>step2 =DeleteCases[step1,0，{2}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,2,2,1,2,0},{1,1,0},{1,2,0},{1,3,1,1,2}}</nowiki>}}
</code>

最后，上面的操作会由单个0元素的列表产生空列表（不可能存在包含多个零的子列表\[LongDash]\[LongDash]为什么？）。我们必须也将它们删除：

<code>
{{SourceCodeIn|code=<nowiki>step3=DeleteCases[step2,{}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,2,2,1,2,0},{1,1,0},{1,2,0},{1,3,1,1,2}}</nowiki>}}
</code>

现在将所有步骤打包成一个函数：

<code>
{{SourceCode|code=<nowiki>Clear[nonzeroSubsequences];
nonzeroSubsequences[x:{__Integer}]:=
	DeleteCases[DeleteCases[Split[x,#1 != 0&], 0, {2}], {}]</nowiki>}}
</code>

注意这里命名模式和 BlankSequence 的使用可以更好的限制参数。测试：

<code>
{{SourceCodeIn|code=<nowiki>nonzeroSubsequences[testdata]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{2,2,2,1,2},{1,1},{1,2},{1,3,1,1,2}}</nowiki>}}
</code>

==== Cases 和 DeleteCases：相同点和不同点 ====

在语法方面，DeleteCases的所有操作都和 Cases 十分类似。然后它们通常被用于逻辑上完全不同的情况。Cases 通常用于提取出表达式某些部分（子表达式），我们一般并不关心表达式本身在之后怎么样了。反之，DeleteCases用于对表达式本身进行结构性修改（我说的表达式本身指的是由DeleteCases内部产生输入的拷贝，因为在 Mathematica 中一般不会引入副作用，原输入也不会有任何形式的修改），而不关心我们删除的部分后来怎么样了。这样看来，Cases 和 DeleteCases 实际上是正好相反的。

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];</nowiki>}}</code>

=== MemberQ ===

这个函数用于判断某个对象（通常是模式）是否为某个表达式的一部分。这里的对象为模式时，它给出是否存在匹配此模式的子表达式。格式为

MemberQ[expr, pattern, levspec], 

同样，这里可选的第三个参数<levspec> 是层级指定。例子：

==== 例子：检查是否存在质数 ====

<code>
{{SourceCodeIn|code=<nowiki>MemberQ[{4,6,8,9,10},_?PrimeQ]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>False</nowiki>}}
</code>

由于这里没有质数，所以结果为 False。

==== 例子：测试符号列表的成员性 ====

<code>
{{SourceCodeIn|code=<nowiki>Clear[a,b,c,d,e];

MemberQ[{a,b,c,d,e},a]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>True</nowiki>}}
</code>

==== 例子：测试嵌套列表的成员性 ====

考虑我们之前提到过的那个关于嵌套列表的例子：

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist];

testlist=Range/@Range[6]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{1},{1,2},{1,2,3},{1,2,3,4},{1,2,3,4,5},{1,2,3,4,5,6}}</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>MemberQ[testlist,_Integer]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>False</nowiki>}}
</code>

这是由于默认只会考虑首层。然而，

<code>
{{SourceCodeIn|code=<nowiki>MemberQ[testlist,_List]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>True</nowiki>}}
</code>

这又是由于首层的成员都是子列表。当我们查看第二层时：

<code>
{{SourceCodeIn|code=<nowiki>{MemberQ[testlist,_Integer,{2}],MemberQ[testlist,_List,{2}]}</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{True,False}</nowiki>}}
</code>

结果正好相反，因为第二层全都是数字。如果我们同时查看首层和次层，两种的结果都为 True：

<code>
{{SourceCodeIn|code=<nowiki>{MemberQ[testlist,_Integer,2],MemberQ[testlist,_List,2]}</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{True,True}</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>Clear[testlist]</nowiki>}}</code>

==== 例子：不排序版Intersection ====

我们知道，有个内置函数 Intersection 可以给出两列表的交集（公共部分）。然而它会移除重复的元素并将结果重新排序，这并不总是我们期待的行为。我们可以结合 Cases 和 MemberQ 写成我们自己的版本，这个版本不会对结果进行排序也不会删除相同的元素。所以简单来说：对于=给定的两列表，我们必须让第一个列表中只留下在第二个列表里也出现的元素。

下面是我们的列表：

<code>
{{SourceCodeIn|code=<nowiki>list1=Table[Random[Integer,{1,30}],{20}]

list2=Table[Random[Integer,{1,30}],{20}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{3,27,21,25,19,10,23,24,21,19,10,23,27,26,22,23,5,3,2,21}</nowiki>}}
</code>

<code>
{{SourceCodeOut|code=<nowiki>{23,11,25,29,8,9,2,8,13,8,27,20,29,30,20,13,28,11,13,12}</nowiki>}}
</code>

这是一种解法：

<code>
{{SourceCodeIn|code=<nowiki>Cases[list1,x_/;MemberQ[list2,x]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{27,25,23,23,27,23,2}</nowiki>}}
</code>

注意这里还有一个不使用 MemberQ 而使用可选模式的版本：

<code>
{{SourceCodeIn|code=<nowiki>Cases[list1,Apply[Alternatives,list2]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{27,25,23,23,27,23,2}</nowiki>}}
</code>

Apply 操作符的含义将在第5章解释，它在这里基本上用于从列表生成一个大型可选模式：

<code>
{{SourceCodeIn|code=<nowiki>Apply[Alternatives,list2]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>23|11|25|29|8|9|2|8|13|8|27|20|29|30|20|13|28|11|13|12</nowiki>}}
</code>

可以证明方法二对于大型列表更加高效。但是值得注意的是，两者的复杂度一般来说都是 L1*L2，这里的L1和L2是列表的长度。由于 Cases 和 MemberQ 是被优化过的，上面的方法必然比嵌套循环要快，导致这项事实可能在某种程度上被隐藏起来。They work well as "scriping" solutions for small lists, 但是对于大型列表还存在更高效的实现。我们将在第6章详细讨论此事。

<code>
{{SourceCodeIn|code=<nowiki>Clear[list1,list2];</nowiki>}}</code>

=== 重访 Position ===

在列表那一章我们已经讨论过这个指令了。由于当时没介绍到模式，我们的讨论受到了相当的限制。事实上，由于它能工作于任意模式，并且返回表达式中模式被匹配的全部位置，Position 是个通用得多的操作符。我们来考虑几个例子。

==== 例子：被三整数的数的位置 ====

<code>
{{SourceCodeIn|code=<nowiki>Range[30]

Position[Range[30],x_/;Mod[x,3]==0]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30}</nowiki>}}
</code>

<code>
{{SourceCodeOut|code=<nowiki>{{3},{6},{9},{12},{15},{18},{21},{24},{27},{30}}</nowiki>}}
</code>

==== 符号表达式上的例子 ====

<code>
{{SourceCodeIn|code=<nowiki>expr=Expand[(1+x)^10]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>1+10 x+45 x^2+120 x^3+210 x^4+252 x^5+210 x^6+120 x^7+45 x^8+10 x^9+x^10</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>FullForm[expr]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>Plus[1,Times[10,x],Times[45,Power[x,2]],Times[120,Power[x,3]],Times[210,Power[x,4]],Times[252,Power[x,5]],Times[210,Power[x,6]],Times[120,Power[x,7]],Times[45,Power[x,8]],Times[10,Power[x,9]],Power[x,10]]</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>Position[expr, x^_?OddQ]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{4,2},{6,2},{8,2},{10,2}}</nowiki>}}
</code>

返回结果是 x的奇数幂（不包括 x 本身）在表达式<expr>中的位置。我们可以 Extract 检查此判断：

<code>
{{SourceCodeIn|code=<nowiki>Extract[expr,Position[expr,x^_?OddQ]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{x^3,x^5,x^7,x^9}</nowiki>}}
</code>

等价地，我们也可使用 Cases ：

<code>
{{SourceCodeIn|code=<nowiki>Cases[expr,x^_?OddQ,Infinity]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{x^3,x^5,x^7,x^9}</nowiki>}}
</code>

然而，有了位置列表我们可以做到更多。例如，如果我们移除位置列表中每个位置的最后一个指标，这些指数就包含在结果的一部分里了：

<code>
{{SourceCodeIn|code=<nowiki>Extract[expr,

Position[expr,x^_?OddQ]/.{y__Integer,z_Integer}:>{y}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{120 x^3,252 x^5,120 x^7,10 x^9}</nowiki>}}
</code>

一定要确保搞懂刚刚我们做了什么。顺便说一句，上面的代码演示了表达式拆解的另一种途径（不是纯模式），混合了结构式和基于模式的方法。

=== Count ===

Count 函数通过 Count[expr, pattern, levspec] 给出<expr> 里匹配 <pattern> 的子表达式出现的次数，层级指定由可选的第三个参数<levspec>给出。例子：

==== 简单例子 ====

给出前30个自然数中被6整除的数的个数：

<code>
{{SourceCodeIn|code=<nowiki>Count[Range[30],x_/;Mod[x,6]==0]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>5</nowiki>}}
</code>

==== 例子：某个字母在一个句子中的出现次数 ====

某个短语里有很多字母 "s"：

<code>
{{SourceCodeIn|code=<nowiki>chars=Characters["she sells sea shells on the sea shore"]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{s,h,e, ,s,e,l,l,s, ,s,e,a, ,s,h,e,l,l,s, ,o,n, ,t,h,e, ,s,e,a, ,s,h,o,r,e}</nowiki>}}
</code>

<code>
{{SourceCodeIn|code=<nowiki>Count[chars,"s"]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>8</nowiki>}}
</code>

短语中所有不相同的字符和其出现次数：

<code>
{{SourceCodeIn|code=<nowiki>alphabet=Union[chars];

freqchars=Table[{alphabet[[i]],Count[chars,alphabet[[i]]]},{i,Length[alphabet]}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{ ,7},{a,2},{e,7},{h,4},{l,4},{n,1},{o,2},{r,1},{s,8},{t,1}}</nowiki>}}
</code>

将此列表按次数升序排列：

<code>
{{SourceCodeIn|code=<nowiki>Sort[freqchars,#1[[2]]<#2[[2]]&]

(*【编译者按】新版本中也可写作SortBy[freqchars, Last]*)</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{t,1},{r,1},{n,1},{o,2},{a,2},{l,4},{h,4},{e,7},{ ,7},{s,8}}</nowiki>}}
</code>

注意这里自定义排序函数的使用，它比较每个子列表的第二个成员（次数）。这里用到的纯函数将马上介绍。

<code>
{{SourceCodeIn|code=<nowiki>Clear[chars,freqchars,alphabet];</nowiki>}}</code>

同时回忆起在奇子序列的提取（章节 3.6.8.4）的那个问题中我们也曾使用过 Count。

=== FreeQ ===

此命令测试给定的表达式是否完全"逃离"了某个符号或者子表达式，也就是说不包含这样的一个子表达式。语法格式是 FreeQ[expr, pattern]。如其名所示，FreeQ是个返回 True 或者 False 的谓词。它在必须对某对象定义新规则的时候十分有用，一个经典的例子是用户自定义的求导函数（见帮助）。FreeQ的一个特性是它也会测试（子）表达式的头部，因为它有一个默认值为 True 的 Heads 选项（参加下面关于 Heads 选项的注解）。

作为例子，假设我们想定义数据类型 Matrix， 以及作用于此数据类型的乘法算符，这个算符只在头部为 Heads 的项上起作用，被排除的项上可进行标准的可交换乘法，我们的新乘法命令里面只有 <Matrix> 对象会保持不变。下面是实现方法：

<code>
{{SourceCode|code=<nowiki>Clear[ourTimes, Matrix];

ourTimes[a__, x__Matrix, b___] /; FreeQ[{a}, Matrix]:=

	Times[a, ourTimes[x,b]];

ourTimes[a___, x__Matrix, b__, c___] /; FreeQ[{b},Matrix]:=

	Times[b, ourTimes[a, x, c]];</nowiki>}}
</code>

例如：

<code>
{{SourceCodeIn|code=<nowiki>ourTimes[a,Matrix[{1,2},{3,4}],b ,c,

Matrix[{5,6},{7,8}],d,Matrix[{9,10},{11,12}]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>a b c d ourTimes[Matrix[{1,2},{3,4}],Matrix[{5,6},{7,8}],Matrix[{9,10},{11,12}]]</nowiki>}}
</code>

=== 对于 Heads 选项的注解 ===

很多 Mathematica 内置函数，特别是所有刚才提到的函数，都拥有Heads选项，它可被设定为 Heads -> True 或者 Heads -> False。对于大多数函数（Position 和 FreeQ 是例外），默认的选项为 Heads -> False。这种情况下，表达式的头部不在搜索和处理的范围内，它们对于这些函数是"透明"的。然而， Heads -> True 选项会让它们变得可见并且被当做其他表达式一样对待。这里我不会涉及太多的细节，我要说明的是在某些情况下这两个选项的区别十分重要。在帮助文档和 《Mathematica 全书》可以找到一些例子。

=== 更复杂的例子—搜寻子序列 ===

==== 不含显式模式的例子 ====

考虑下面的问题：我们想知道数字组合<123>是否会出现在 \[Pi] 的前500位里。为此，我们诉诸于 RealDigits 命令。例如，\[Pi] 的前20位可由下面的方法给出：

<code>
{{SourceCodeIn|code=<nowiki>RealDigits[N[Pi,20]][[1]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{3,1,4,1,5,9,2,6,5,3,5,8,9,7,9,3,2,3,8,4}</nowiki>}}
</code>

我们问题的解决方法看起来是这样的：

<code>
{{SourceCodeIn|code=<nowiki>MemberQ[{RealDigits[N[Pi,500]][[1]]},{x___,1,2,3,y___}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>False</nowiki>}}
</code>

那么前1000位呢？

<code>
{{SourceCodeIn|code=<nowiki>MemberQ[{RealDigits[N[Pi,1000]][[1]]},{x___,1,2,3,y___}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>False</nowiki>}}
</code>

前2000位？

<code>
{{SourceCodeIn|code=<nowiki>MemberQ[{RealDigits[N[Pi,2000]][[1]]},{x___,1,2,3,y___}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>True</nowiki>}}
</code>

最后的答案是存在，但是找到的组合到底在哪里？ MemberQ 不会回答这个问题，我们需要进一步努力：

<code>
{{SourceCodeIn|code=<nowiki>Position[Partition[RealDigits[N[Pi,2000]][[1]],3,1],{1,2,3}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{1925}}</nowiki>}}
</code>

为了理解其工作过程，考虑下面的迷你例子，即将数字列表切到前20位：

<code>
{{SourceCodeIn|code=<nowiki>RealDigits[N[Pi,20]][[1]]

Partition[RealDigits[N[Pi,20]][[1]],3,1]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{3,1,4,1,5,9,2,6,5,3,5,8,9,7,9,3,2,3,8,4}</nowiki>}}
</code>

<code>
{{SourceCodeOut|code=<nowiki>{{3,1,4},{1,4,1},{4,1,5},{1,5,9},{5,9,2},{9,2,6},{2,6,5},{6,5,3},{5,3,5},{3,5,8},{5,8,9},{8,9,7},{9,7,9},{7,9,3},{9,3,2},{3,2,3},{2,3,8},{3,8,4}}</nowiki>}}
</code>

我们得到一组长度为 3 偏移量为 1 的"滑动" 子列表。然后 Position 函数搜索成员 {1, 2, 3} 并返回其位置。显然，所得的位置正好是 1, 2, 3 中的 1 在原列表中的位置，因为"滑动"的偏移量仅为一个单位（否则就不是这样）。我们可以直接用 Take 验证：

<code>
{{SourceCodeIn|code=<nowiki>digits = RealDigits[N[Pi,2000]][[1]];

pos = First[First[Position[Partition[digits,3,1],{1,2,3}]]]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>1925</nowiki>}}
</code>

现在取出此位置开始的数字序列：

<code>
{{SourceCodeIn|code=<nowiki>Take[digits,{pos,pos+2}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{1,2,3}</nowiki>}}
</code>

让我来做个一般性的评述。这个例子中，我们见识到了编写和测试一个 Mathematica 函数的典型方法：在编写函数之前或者是所写的函数出现错误的时候，我们切取此函数所作用的列表的前几个元素，然后在这个小列表上解决所有问题。对于复杂和嵌套的列表（树），此过程可能会变得不那么简单，这其实无关紧要，只要小型测试列表的结构和原大型列表保持一致就行了。

现在考虑10000位的情况：

<code>
{{SourceCodeIn|code=<nowiki>Position[Partition[RealDigits[N[Pi,10000]][[1]],3,1],{1,2,3}]//Timing</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{0.,{{1925},{2939},{2977},{3893},{6549},{7146},{8157},{8773},{8832},{9451},{9658}}}</nowiki>}}
</code>

以上就是 \[Pi] 的前10000位中出现 <123> 组合的位置。

==== 一个类似的例子 ====

作为另外一个例子，我们找到 \[Pi] 的前 1000 位中以<1>开始以<9>结尾的三个数字的组合。代码如下；

<code>
{{SourceCodeIn|code=<nowiki>digits= RealDigits[N[Pi,1000]][[1]];

partdigits=Partition[digits,3,1];

pos=Position[partdigits,{1,_,9}]

Extract[partdigits,pos]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{4},{41},{207},{439},{495},{500},{526},{548},{705},{713},{731},{923},{985},{998}}</nowiki>}}
</code>

<code>
{{SourceCodeOut|code=<nowiki>{{1,5,9},{1,6,9},{1,0,9},{1,7,9},{1,1,9},{1,2,9},{1,3,9},{1,7,9},{1,9,9},{1,2,9},{1,5,9},{1,5,9},{1,1,9},{1,9,9}}</nowiki>}}
</code>

或者

<code>
{{SourceCodeIn|code=<nowiki>Cases[partdigits,{1,_,9}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{1,5,9},{1,6,9},{1,0,9},{1,7,9},{1,1,9},{1,2,9},{1,3,9},{1,7,9},{1,9,9},{1,2,9},{1,5,9},{1,5,9},{1,1,9},{1,9,9}}</nowiki>}}
</code>

将所有的数字组合和其位置放在一起可能更有趣。首先想到的是将数字列表和其位置列表组合在一起然后进行转置(Transpose)：

<code>
{{SourceCodeIn|code=<nowiki>indexedcombs=Transpose[{Extract[partdigits,pos],pos}]</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{{1,5,9},{4}},{{1,6,9},{41}},{{1,0,9},{207}},{{1,7,9},{439}},{{1,1,9},{495}},{{1,2,9},{500}},{{1,3,9},{526}},{{1,7,9},

                   {548}},{{1,9,9},{705}},{{1,2,9},{713}},{{1,5,9},{731}},{{1,5,9},{923}},{{1,1,9},{985}},{{1,9,9},{998}}}</nowiki>}}
</code>

如果有人想取出位置两边的花括号的话，一条简单的规则就能搞定：

<code>
{{SourceCodeIn|code=<nowiki>indexedcombs/.{x_Integer}->x</nowiki>}}</code>

<code>
{{SourceCodeOut|code=<nowiki>{{{1,5,9},4},{{1,6,9},41},{{1,0,9},207},{{1,7,9},439},{{1,1,9},495},{{1,2,9},500},{{1,3,9},526},{{1,7,9},548},{{1,9,9},705},

            {{1,2,9},713},{{1,5,9},731},{{1,5,9},923},{{1,1,9},985},{{1,9,9},998}}</nowiki>}}
</code>
